import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@XmlRootElement(name = "text")
@XmlAccessorType(XmlAccessType.FIELD)
public class SentenceList {

    private Collection<Sentence> sentence = null;

    public Collection<Sentence> getSentence() {
        return sentence;
    }

    public void setSentence(Collection<Sentence> sentence) {
        this.sentence = sentence;
    }
}
