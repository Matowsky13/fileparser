import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileToXMLSentenceTest {
    private List<Sentence> listOfSentence = new ArrayList<>();

    @Before
    public void setUp() {
        Sentence firstSentence = new Sentence(Collections.singletonList("test test01 test02. test03"));
        Sentence secondSentence = new Sentence(Collections.singletonList("marshaller01 marshaller02 marshaller03. marshaller04"));
        listOfSentence.add(firstSentence);
        listOfSentence.add(secondSentence);
    }

    @Test
    public void testObjectToXml() throws JAXBException {
        SentenceList sentenceList = new SentenceList();
        sentenceList.setSentence(listOfSentence);
        JAXBContext jaxbContext = JAXBContext.newInstance(SentenceList.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(sentenceList, new File(FileParser.PATH + "testXMLFile.xml"));
    }
}