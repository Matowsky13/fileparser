import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

public class FileParser {
    private static final String DELIMITER = " ";
    private static final String DOT = ".";
    private static final String SUFFIX = "Mr.";
    private static final String EXCLAMATION_MARK = "!";
    private static final String QUESTION_MARK = "?";
    private static final String CSV_SEPARATOR = ",";
    private static final String TAB = "\t";
    private static final String EMPTY_SPACE = "";
    public static final String PATH = "C:\\Workspace\\FileParser\\input\\";

    public static void main(String[] args) throws IOException, JAXBException {
        //File names to choose: SMALL or LARGE
        Path parseFilesDirectory = Paths.get(PATH + FileNames.SMALL.getFileName());
        List<Sentence> values = new ArrayList<>(parseFile(parseFilesDirectory).values());
        //Create XML file from input file
        createXMLFile(values);
        //Create CSV file from input file
        createCSVFile(values);
    }

    public static Map<Integer, Sentence> parseFile(Path fileDirectory) throws IOException {
        List<String> textLines = Files.readAllLines(fileDirectory);
        Map<Integer, Sentence> sentenceMap = new HashMap<>();
        List<String> listOfWords = new ArrayList<>();
        int count = 0;
        for (String singleSentence : textLines) {
            String[] splitWord = singleSentence.replace(TAB, DELIMITER).split(DELIMITER);
            for (String singleWord : splitWord) {
                if (!singleWord.isEmpty()) {
                    listOfWords.add(singleWord.replaceAll("[^a-zA-Z]", EMPTY_SPACE));
                }
                if (singleWord.endsWith(DOT) || singleWord.endsWith(EXCLAMATION_MARK) || singleWord.endsWith(QUESTION_MARK)) {
                    if (!singleWord.matches(SUFFIX)) {
                        Sentence sentence = new Sentence();
                        listOfWords.sort(String::compareToIgnoreCase);
                        sentence.setListOfWords(listOfWords);
                        sentenceMap.put(count++, sentence);
                        listOfWords = new ArrayList<>();
                    }
                }
            }
        }
        return sentenceMap;
    }

    private static void createXMLFile(List<Sentence> listOfSentences) throws JAXBException {
        SentenceList sentenceList = new SentenceList();
        sentenceList.setSentence(listOfSentences);

        JAXBContext jaxbContext = JAXBContext.newInstance(SentenceList.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(sentenceList, new File(PATH + FileNames.XML.getFileName()));
    }

    private static void createCSVFile(List<Sentence> sentenceArrayList) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PATH + FileNames.CSV.getFileName()), StandardCharsets.UTF_8));
            for (Sentence sentence : sentenceArrayList) {
                String sentenceLine = sentence.getListOfWords() + CSV_SEPARATOR;
                bw.write(sentenceLine.replace("[", "").replace("]", ""));
                bw.newLine();
            }
            bw.flush();
            bw.close();
        } catch (IOException ignored) {
        }
    }
}
