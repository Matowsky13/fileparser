public enum FileNames {
    //Input
    SMALL("small.txt"),
    LARGE("large.txt"),
    //Output
    XML("sentenceXML.xml"),
    CSV("sentenceCSV.csv");

    private final String fileName;

    FileNames(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
